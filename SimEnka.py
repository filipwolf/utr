import fileinput

dict1 = {}
fakatKon = ""


def ispis(konacnoStanje, pocStanje, i):
    
    konIspis = ""
    stPrev = ""
    global fakatKon

    for stKon in konacnoStanje:
        if stKon == "|" or stKon in pocStanje[0] or stPrev == "|":
            konIspis = konIspis + stKon
        else:
            konIspis = konIspis + "," + stKon
        stPrev = stKon

    fakatKon = fakatKon + "\n" + konIspis
    if i == (len(dict1[0]) - 1):
        print(fakatKon)


def pisi():
    for name in dict1.items():
        print(name)


def main():

    cnt = 0

    for line in fileinput.input():
        dict2 = {}
        j = 0
        fields = line.split("|")
        for i in fields:
            dict2[j] = i.rstrip()
            j = j + 1
        dict1[cnt] = dict2
        cnt = cnt + 1

    pocSt = dict1[4][0]

    #ispis po svim unosima
    for i in range(len(dict1[0])):

        trenStan = [pocSt]
        epsilonStan = [pocSt]
        novaStanja = []
        stanPrivr = []
        pocStanje = []
        finalStanje = []
        konacnoStanje = []
        vecProvjerio = []
        ulazniZnakovi = dict1[0][i].split(",")
        k = 0

        #samo za poc stanje epsilon prijelaza
        while k < len(epsilonStan):
            for st in range(5, cnt):
                stanjeElem = dict1[st][0].split("->")
                stanjeLijevo = stanjeElem[0].split(",")
                if stanjeLijevo[0] != stanjeElem[1]:
                    if stanjeLijevo[0] == epsilonStan[k] and stanjeLijevo[1] == "$":

                        if "," in stanjeElem[1]:
                            nekaj = stanjeElem[1].split(",")
                            for elem in nekaj:
                                epsilonStan.append(elem)
                        else:
                            epsilonStan.append(stanjeElem[1])
                        break

            k += 1
            trenStan = epsilonStan.copy()
            trenStan.sort()
            pocStanje = trenStan.copy()

        konacnoStanje.extend(trenStan)

        #iteriramo po ulaznim znakovima
        for ulZnak in ulazniZnakovi:
            nekajNovo = []
            novaStanja = []
            stanPrivr = []
            nekajNovo = []
            k = 0
            flag = 0
            vecProvjerio = []

            #iteriramo po stanjima
            for stanje in trenStan:
                k = 0

                #trazimo postoji li stanje za dani znak
                for st in range(5, cnt):
                    stanjeElem = dict1[st][0].split("->")
                    stanjeLijevo = stanjeElem[0].split(",")
                    if stanjeElem[1] != "#":
                        if stanjeLijevo[0] == stanjeElem[1] and stanjeLijevo[1] == "$":
                            continue
                        else:
                            if stanje == stanjeLijevo[0] and ulZnak == stanjeLijevo[1]:
                                flag = 1
                                if "," in stanjeElem[1]:
                                     nekaj = stanjeElem[1].split(",")
                                     for elem in nekaj:
                                         stanPrivr.append(elem)
                                else:
                                    stanPrivr.append(stanjeElem[1])

                #optimizacija
                epsilonStan.clear()
                for bla in stanPrivr:
                    if bla not in nekajNovo:
                        epsilonStan.append(bla)
                nekajNovo.extend(stanPrivr)
                nekajNovo = list(set(nekajNovo))

                #trazimo epsilon stanja
                while k < len(epsilonStan):

                    for st in range(5, cnt):
                        stanjeElem = dict1[st][0].split("->")
                        stanjeLijevo = stanjeElem[0].split(",")
                        if stanjeElem[1] != "#":
                            if stanjeLijevo[0] in vecProvjerio:
                                continue
                            else:
                                if stanjeLijevo[0] != stanjeElem[1]:
                                    if stanjeLijevo[0] == epsilonStan[k] and stanjeLijevo[1] == "$":
                                        vecProvjerio.append(stanjeLijevo[0])
                                        if "," in stanjeElem[1]:
                                            nekaj = stanjeElem[1].split(",")
                                            for elem in nekaj:
                                                epsilonStan.append(elem)
                                                novaStanja.append(elem)
                                        else:
                                            epsilonStan.append(stanjeElem[1])
                                            novaStanja.append(stanjeElem[1])
                                        break
                    k += 1

            stanPrivr.extend(novaStanja)
            stanPrivr = list(set(stanPrivr))
            trenStan = stanPrivr.copy()
            trenStan.sort()
            finalStanje = trenStan.copy()
            konacnoStanje.append("|")
            if flag == 0:
                konacnoStanje.extend("#")
            else:
                konacnoStanje.extend(finalStanje)

        ispis(konacnoStanje, pocStanje, i)

main()

