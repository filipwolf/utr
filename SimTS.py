import fileinput

dict = {}

trenSt = ''
traka = ''
trenZnak = 0
prihv = 0

def ispis():

    global prihv

    prihvStanja = prihv.split(',')

    if trenSt in prihvStanja:
        prihvatljivost = 1
    else:
        prihvatljivost = 0

    finalString = str(trenSt) + '|' + str(trenZnak) + '|' + str(traka) + '|' + str(prihvatljivost)

    print(finalString)

def main():

    global trenZnak
    global trenSt
    global traka
    global prihv

    j = 0

    for line in fileinput.input():
        #print(line)
        dict[j] = line.rstrip()
        j += 1
    #print(dict)

    trenZnak = int(dict[7])
    traka = dict[4]
    trenSt = dict[6]
    prihv = dict[5]

    flag = True

    while(flag == True):

        flag = False

        for i in range(8, len(dict)):

            split1 = dict[i].split("->")
            lijevo = split1[0].split(",")
            desno = split1[1].split(",")

            if traka[trenZnak] == lijevo[1] and trenSt == lijevo[0]:

                flag = True
                trenSt = desno[0]
                listaTraka = list(traka)
                listaTraka[trenZnak] = desno[1]
                traka = ''.join(listaTraka)

                if desno[2] == 'R':
                    trenZnak += 1
                else:
                    trenZnak -= 1

                if trenZnak < 0 or trenZnak > len(traka) - 1:
                    if desno[2] == 'R':
                        trenZnak -= 1
                    else:
                        trenZnak += 1

                    return

                break





main()
ispis()